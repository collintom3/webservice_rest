package com.example.webservices.entity;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name="articles")
public class Article {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column()
    private String designation;
    private Float price;
    private Long quantity;

    @OneToMany(mappedBy = "article", cascade = CascadeType.ALL)
    private List<ArticleOrder> articleOrders;

}
