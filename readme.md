# Mon Projet API REST avec Spring Boot et PostgreSQL

Ce projet est une application API REST construite avec Spring Boot et PostgreSQL. Il est conteneurisé pour un déploiement facile et consistant avec Docker.

## Prérequis

- [Java 11](https://adoptopenjdk.net/)
- [Gradle](https://gradle.org/install/)
- [Docker](https://www.docker.com/products/docker-desktop)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Lancement du projet

1. Lancez Docker :
```bash
sudo service docker start
```

2. Construisez et démarrez les conteneurs avec Docker Compose :
```bash
docker-compose up
```

3. Lancez votre api sur intelliJ

Une fois ces commandes exécutées, votre API REST devrait être opérationnelle et accessible à l'adresse http://localhost:8080.

## Utilisation de l'API

Dans le dossier ressources vous trouverez un fichier json que vous pourrez impoter dans postman, cela vous donnera des exemples de requêtes à effectuer.

## Schéma de la base de données

![alt text](ressources/bdd.png)