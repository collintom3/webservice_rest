package com.example.webservices.entity;


import jakarta.persistence.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column()
    private Date date;

    @PrePersist
    private void createdAt() {
        this.date = new Date();
    }

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    public List<ArticleOrder> articleOrders = new ArrayList<>();
}
