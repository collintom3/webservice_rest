package com.example.webservices.api;

import com.example.webservices.api.dto.ArticleDto;
import com.example.webservices.api.dto.ArticleOrderDto;
import com.example.webservices.api.dto.OrderDto;
import com.example.webservices.entity.Order;
import com.example.webservices.service.OrderService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping(
        path = "/orders",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class OrderController {
    private final OrderService orderService;

    //@ApiOperation(value = "Allows you to create an order")
    @GetMapping
    public ResponseEntity<List<OrderDto>> getAll() {
        return ResponseEntity.ok(this.orderService.getAll()
                .stream()
                .map(this::mapOrderToDto)
                .collect(Collectors.toList()));
    }

    //@ApiOperation(value = "Allows you to retrieve the order by the id")
    @GetMapping(path = "{id}")
    public ResponseEntity<OrderDto> getById(@PathVariable Long id) {
        return this.orderService.getById(id)
                .map(order -> ResponseEntity.ok(mapOrderToDto(order)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrderDto> create(@RequestBody List<ArticleOrderDto> articleOrderDtoList) {
        try {
            return ResponseEntity.status(HttpStatus.CREATED).body(mapOrderToDto(this.orderService.create(articleOrderDtoList)));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
    }

    @PutMapping(path = "{id}", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<OrderDto> update(@PathVariable Long id, @RequestBody List<ArticleOrderDto> articleOrderDtoList) {
        try {
            return ResponseEntity.ok(mapOrderToDto(this.orderService.update(id, articleOrderDtoList)));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    //delete order with id in parameter
    @DeleteMapping(path = "{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        try {
            this.orderService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    private OrderDto mapOrderToDto(Order order) {
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setArticles(order.getArticleOrders()
                .stream()
                .map(orderArticle -> {
                    ArticleDto articleOrderDto = new ArticleDto();
                    articleOrderDto.setId(orderArticle.getArticle().getId());
                    articleOrderDto.setDesignation(orderArticle.getArticle().getDesignation());
                    articleOrderDto.setPrice(orderArticle.getArticle().getPrice());
                    articleOrderDto.setQuantity(orderArticle.getQuantity());
                    return articleOrderDto;
                }).collect(Collectors.toList()));
        return orderDto;
    }
}
