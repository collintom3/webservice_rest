package com.example.webservices.api;

import com.example.webservices.api.dto.ArticleDto;
import com.example.webservices.entity.Article;
import com.example.webservices.service.ArticleService;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@RestController
@RequestMapping(
        path = "/articles",
        produces = {MediaType.APPLICATION_JSON_VALUE}
)
public class ArticleController {
    private final ArticleService articleService;

    //@ApiOperation(value = "Allows you to retrieve the list of sites")
    @GetMapping
    public ResponseEntity<List<ArticleDto>> getAll() {
        return ResponseEntity.ok(this.articleService.getAll()
                .stream()
                .map(this::mapToDto)
                .collect(Collectors.toList()));
    }

    //@ApiOperation(value = "Allows you to retrieve the site by the id")
    @GetMapping(path = "{id}")
    public ResponseEntity<ArticleDto> getById(@PathVariable Long id) {
        return this.articleService.getById(id)
                .map(article -> ResponseEntity.ok(mapToDto(article)))
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArticleDto> create(@RequestBody ArticleDto articleDto) {
        return ResponseEntity.ok(mapToDto(this.articleService.create(articleDto.getDesignation(), articleDto.getPrice(), articleDto.getQuantity())));
    }

    @PutMapping(path = "{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArticleDto> update(@PathVariable("id") Long id, @RequestBody ArticleDto articleDto) {
        try {
            return ResponseEntity.ok(mapToDto(this.articleService.update(id, articleDto.getDesignation(), articleDto.getPrice(), articleDto.getQuantity())));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @DeleteMapping(path = "{id}")
    public ResponseEntity<Long> deleteById(@PathVariable("id") Long id) {
        try {
            this.articleService.deleteById(id);
            return ResponseEntity.accepted().body(id);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }


    private ArticleDto mapToDto(Article article) {
        ArticleDto articleDto = new ArticleDto();
        articleDto.setId(article.getId());
        articleDto.setDesignation(article.getDesignation());
        articleDto.setPrice(article.getPrice());
        articleDto.setQuantity(article.getQuantity());
        return articleDto;
    }
}
