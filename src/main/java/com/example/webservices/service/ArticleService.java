package com.example.webservices.service;

import com.example.webservices.entity.Article;
import com.example.webservices.repository.ArticleRepository;
import com.example.webservices.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class ArticleService {
    private final ArticleRepository articleRepository;
    private final OrderRepository orderRepository;

    public List<Article> getAll() {
        return this.articleRepository.findAll();
    }

    public Optional<Article> getById(Long id) {
        return this.articleRepository.findById(id);
    }

    public Article create(String designation, Float price, Long quantity) {
        Article article = new Article();
        article.setDesignation(designation);
        article.setPrice(price);
        article.setQuantity(quantity);
        return this.articleRepository.save(article);
    }

    public Article update(Long id, String designation, Float price, Long quantity) throws Exception {
        Optional<Article> article = this.articleRepository.findById(id);
        if(article.isPresent()) {
            if(designation != null) {
                article.get().setDesignation(designation);
            }
            if(price != null) {
                article.get().setPrice(price);
            }
            if(quantity != null) {
                article.get().setQuantity(quantity);
            }
            return this.articleRepository.save(article.get());
        }
        throw new Exception("Id not found");
    }


    public void deleteById(Long id) throws Exception {
        if(!this.articleRepository.findById(id).isEmpty()) {
            this.articleRepository.deleteById(id);
        }
        throw new Exception("Id not found");
    }

}
