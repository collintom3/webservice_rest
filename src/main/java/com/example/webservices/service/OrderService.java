package com.example.webservices.service;

import com.example.webservices.api.dto.ArticleOrderDto;
import com.example.webservices.entity.Article;
import com.example.webservices.entity.ArticleOrder;
import com.example.webservices.entity.Order;
import com.example.webservices.repository.ArticleRepository;
import com.example.webservices.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final ArticleRepository articleRepository;

    public List<Order> getAll() {
        return this.orderRepository.findAll();
    }

    public Optional<Order> getById(Long id) {
        return this.orderRepository.findById(id);
    }


    public Order create(List<ArticleOrderDto> articleOrderDtoList) throws Exception {
        Order order = new Order();
        for(ArticleOrderDto articleOrderDto : articleOrderDtoList) {
            if(this.articleRepository.findById(articleOrderDto.getId()).isEmpty()) {
                throw new Exception("Article not found");
            }

            ArticleOrder articleOrder = new ArticleOrder();
            articleOrder.setArticle(this.articleRepository.findById(articleOrderDto.getId()).get());
            articleOrder.setQuantity(articleOrderDto.getQuantity());
            articleOrder.setOrder(order);
            order.getArticleOrders().add(articleOrder);

            Optional<Article> optionalArticle = this.articleRepository.findById(articleOrderDto.getId());
            if(optionalArticle.isEmpty()) {
                throw new Exception("Article not found");
            }
            Article article = optionalArticle.get();
            article.setQuantity(article.getQuantity() - articleOrderDto.getQuantity());
            this.articleRepository.save(article);
        }
        return this.orderRepository.save(order);
    }


    public Order update(Long id, List<ArticleOrderDto> articleOrderDtoList) throws Exception {
        Optional<Order> optionalOrder = this.orderRepository.findById(id);
        if(optionalOrder.isEmpty()) {
            throw new Exception("Order not found");
        }
        Order order = optionalOrder.get();
        for(ArticleOrderDto articleOrderDto : articleOrderDtoList) {
            if(this.articleRepository.findById(articleOrderDto.getId()).isEmpty()) {
                throw new Exception("Article not found");
            }
            //Si l'article n'est pas dans la commande, on l'ajoute
            Optional<ArticleOrder> optionalArticleOrder = this.orderRepository.findById(id).get().getArticleOrders().stream().filter(articleOrder -> articleOrder.getArticle().getId().equals(articleOrderDto.getId())).findFirst();
            if(optionalArticleOrder.isEmpty()) {
                ArticleOrder articleOrder = new ArticleOrder();
                articleOrder.setArticle(this.articleRepository.findById(articleOrderDto.getId()).get());
                if(articleOrder.getQuantity() + articleOrder.getQuantity() - articleOrderDto.getQuantity() < 0) {
                    throw new Exception("Not enough articles in stock");
                }
                articleOrder.setQuantity(articleOrderDto.getQuantity());
                articleOrder.setOrder(this.orderRepository.findById(id).get());
                this.orderRepository.findById(id).get().getArticleOrders().add(articleOrder);
            }
            //Si l'article est dans la commande, on met à jour la quantité en vérifiant si il y en a assez en stock
            else {
                ArticleOrder articleOrder = optionalArticleOrder.get();
                articleOrder.setQuantity(articleOrderDto.getQuantity());
                Optional<Article> optionalArticle = this.articleRepository.findById(articleOrderDto.getId());
                if(optionalArticle.isEmpty()) {
                    throw new Exception("Article not found");
                }
                Article article = optionalArticle.get();
                if(article.getQuantity() + articleOrder.getQuantity() - articleOrderDto.getQuantity() < 0) {
                    throw new Exception("Not enough articles in stock");
                }
                article.setQuantity(article.getQuantity() + articleOrder.getQuantity() - articleOrderDto.getQuantity());
                this.articleRepository.save(article);
            }
        }
        return this.orderRepository.save(order);
    }

    public Order delete(Long id) throws Exception {
        Optional<Order> optionalOrder = this.orderRepository.findById(id);
        if(optionalOrder.isEmpty()) {
            throw new Exception("Order not found");
        }
        Order order = optionalOrder.get();
        for(ArticleOrder articleOrder : order.getArticleOrders()) {
            Article article = articleOrder.getArticle();
            article.setQuantity(article.getQuantity() + articleOrder.getQuantity());
            this.articleRepository.save(article);
        }
        this.orderRepository.delete(order);
        return order;
    }

}